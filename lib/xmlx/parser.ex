defmodule Xmlx.SyntaxError do
  defexception [:message, :token]

  def exception(opts) do
    message = if token = opts[:token] do
      "Unexpected token: #{token}"
    else
      "Unexpected end of input"
    end

    %Xmlx.SyntaxError{message: message, token: token}
  end
end

defmodule Xmlx.Parser do

  alias Xmlx.SyntaxError

  if Application.get_env(:xmlx, :native) do
    @compile :native
  end

  def parse!(string, options \\ []) do
    case parse(string, options) do
      {:ok, value} ->
        value
      {:error, _error} ->
        raise SyntaxError
    end
  end

  def parse(string, _options \\ []) do
    case :erlsom.simple_form(string) do
      {:ok, value, _} ->
        value = value
        |> make_pairs 
        |> compact_pairs
        |> do_map
        {:ok, value}
      error ->
        {:error, error}
    end
  end

  def make_pairs({key, val}) do
    {to_string(key), to_string(val)}
  end

  def make_pairs({key, [], []}) do
    {to_string(key), nil}
  end

  def make_pairs({key, list, []}) do
    val = Enum.map(list, fn(t) -> make_pairs(t) end)
    {to_string(key), val}
  end

  def make_pairs({key, _, [val]}) do
    val = make_pairs(val)
    {to_string(key), val}
  end

  def make_pairs({key, _, list}) do
    val = Enum.map(list, fn(t) -> make_pairs(t) end)
    {to_string(key), val}
  end

  def make_pairs(val) do
    val |> to_string
  end

  def compact_pairs(nil) do
    nil
  end

  def compact_pairs(val) when is_binary(val) do
    val
  end

  def compact_pairs({key, val}) when is_binary(val) do
    {key, val}
  end

  def compact_pairs(val) when is_list(val) do
    repeated_keys = val 
    |> Enum.reduce(%{}, fn(v, acc) -> 
        case v do
          {k, _v} -> 
            exists = Map.has_key?(acc, k)
            acc |> Map.put(k, exists)
          _ ->
            acc
        end
      end)

    v1 = Enum.reject(val, fn(v) ->
      case v do
        {k, _v} ->
          repeated_keys[k] 
        _ ->
          false
      end
    end)

    v2 = repeated_keys
    |> Enum.reduce([], fn({k, v}, acc) ->
        if v do
          vals = Enum.reduce(val, [], fn(vv1, acc1) ->
            case vv1 do
              {^k, vvv1} ->
                acc1 ++ [vvv1]
              _ ->
                acc1
            end
          end)
          acc ++ [{k, vals}]
        else
          acc
        end
      end)

    compact_array_pairs([], v1 ++ v2)
  end

  def compact_pairs({key, val}) do
    val = compact_pairs(val)
    {key, val}
  end

  def compact_array_pairs(state, [h|t]) do
    state = state ++ [compact_pairs(h)]
    state |> compact_array_pairs(t)
  end
  
  def compact_array_pairs(state, []) do
    state
  end

  def do_map(nil) do
    nil
  end

  def do_map(list) when is_list(list) do
    if is_object(list) do
      list 
      |> Enum.map(fn(t) -> do_map(t) end)
      |> Enum.reduce(%{}, fn(v, acc) -> Map.merge(v, acc) end)
    else
      list 
      |> Enum.map(fn(t) -> do_map(t) end)
    end
  end

  def do_map(str) when is_binary(str) do
    str
  end

  def do_map({key, val}) do
    val = do_map(val)
    %{} |> Map.put(key, val)
  end

  def is_object(val) when is_list(val) do
    is_object([], val)
  end

  def is_object(_state, [h|_]) when is_list(h) do
    false
  end

  def is_object(_state, [h|_]) when is_binary(h) do
    false
  end

  def is_object(state, [{k, _v}|t]) do
    if Enum.member?(state, k) do
      false
    else
      state = state ++ [k]
      is_object(state, t)
    end
  end

  def is_object(_state, []) do
    true
  end
end