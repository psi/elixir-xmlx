defmodule ParserBench do
  use Benchfella

  @test_xml """
  <?xml version="1.0" encoding="UTF-8" ?>
  <shopping> 
    <name od="od1">test</name>
    <doudou>
      <my id="1" />
      <my age="1" />
    </doudou>
    <times>
      <time>1927</time>
      <time>1928</time>
      <subtimes>
       <time>1</time>
       <time>2</time>
      </subtimes>
      <subtimes>
       <time>3</time>
       <time>4</time>
      </subtimes>
    </times>
    <item name="bread" quantity="3" price="2.50"/> 
    <item name="奶茶" quantity="2" price="3.50"/>
    <item></item>
    <item /> 
  </shopping>
  """
  bench "Parser.parse" do
    Xmlx.Parser.parse(@test_xml)
  end
end